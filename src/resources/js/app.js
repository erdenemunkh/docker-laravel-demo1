require("./bootstrap");

import { createApp } from "vue";
import App from "./App.vue";
import AllProduct from "./components/AllProduct.vue";
import CreateProduct from "./components/CreateProduct.vue";
import EditProduct from "./components/EditProduct.vue";

// const Home = { template: "<div>Home</div>" };
// const About = { template: "<div>About</div>" };

// const routes = [
//   { path: "/", component: Home },
//   { path: "/about", component: About },
// ];

import { createRouter, createWebHistory } from "vue-router";

const routes = [
    {
        name: "home",
        path: "/",
        component: AllProduct,
    },
    {
        name: "create",
        path: "/create",
        component: CreateProduct,
    },
    {
        name: "edit",
        path: "/edit/:id",
        component: EditProduct,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

createApp({
    components: {
        App,
    },
})
    .use(router)
    .mount("#app");
